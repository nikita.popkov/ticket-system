FROM openjdk:11-jre-slim
COPY --from=build /core/build/libs/ticket.jar /core/ticket.jar
WORKDIR /core
EXPOSE 8080 5000
CMD java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5000 -jar ticket.jar