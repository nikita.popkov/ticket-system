package com.ticket

import com.ticket.api.controllers.UsersController
import org.springframework.beans.factory.getBean
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@EnableMongoRepositories(basePackages = ["com.ticket"])
@ComponentScan(basePackages = ["com.ticket"])
@EntityScan(basePackages = ["com.ticket"])
@ConfigurationPropertiesScan(basePackages = ["com.ticket"])
@SpringBootApplication
class App

fun main(args: Array<String>) {
    val context = runApplication<App>(*args)
    val controller = context.getBean<UsersController>()
}
