package com.ticket.api.controllers

import com.ticket.Response
import com.ticket.usecases.SaveUserUseCase
import com.ticket.usecases.UserSaveData
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

data class UserSaveRequest(
    val firstname: String,
    val lastname: String,
    val middlename: String?
) {
    fun toUserSaveData() = UserSaveData(
        firstname = firstname,
        lastname = lastname,
        middlename = middlename
    )
}

@RestController
@RequestMapping("/users")
class UsersController @Autowired constructor(
    private val saveUserUseCase: SaveUserUseCase
) {

    @PostMapping
    fun saveUser(@RequestBody request: UserSaveRequest): ResponseEntity<Any> {
        runBlocking {
            return@runBlocking when (val resp = saveUserUseCase.execute(SaveUserUseCase.Request(data = request.toUserSaveData()))) {
                is SaveUserUseCase.Response.Success -> {
                    ResponseEntity.ok().body(Response(data = resp.user))
                }
                is SaveUserUseCase.Response.Error -> {
                    ResponseEntity.ok().build()
                }
                is SaveUserUseCase.Response.AccessControlError -> {
                    ResponseEntity.ok().build()
                }
            }
        }
        return ResponseEntity.ok().build()
    }
}
