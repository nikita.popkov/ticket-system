package com.ticket.users

import com.ticket.utils.IdentifiableEntity
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import org.springframework.data.mongodb.core.mapping.MongoId

@Document(collection = "users")
data class User(
    @MongoId val userId: String,
    @Field(name = "firstname") val firstname: String,
    @Field(name = "lastname") val lastname: String,
    @Field(name = "middlename") val middlename: String?
) : IdentifiableEntity {
    override fun id() = userId
}
