package com.ticket.users

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

interface UserRepository : MongoRepository<User, String>

@Repository
abstract class UserRepositoryMongo(
    @Autowired mongoTemplate: MongoTemplate
) : UserRepository
