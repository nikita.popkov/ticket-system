package com.ticket.usecases

import com.ticket.users.User
import com.ticket.users.UserRepository
import com.ticket.utils.IdentityGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

data class UserSaveData(
    val firstname: String,
    val lastname: String,
    val middlename: String?
) {
    fun toNewUser() = User(
        userId = IdentityGenerator.generateSimple(),
        firstname = firstname,
        lastname = lastname,
        middlename = middlename
    )
}

@Component
class SaveUserUseCase : AccessControlledUseCase<SaveUserUseCase.Request, SaveUserUseCase.Response>() {
    override val accessControlPolicy: AccessControlPolicy = AnyoneCanCallPolicy()
    override val accessControlError: Response = Response.AccessControlError

    @Autowired
    private lateinit var userRepository: UserRepository

    data class Request(val data: UserSaveData) : UseCaseRequest
    sealed class Response : UseCaseResponse {
        data class Success(val user: User) : Response()
        object Error : Response()
        object AccessControlError : Response()
    }

    override suspend fun executeControlled(req: Request): Response {
        val newUser = req.data.toNewUser()
        logger().info { "Trying to save new user with id: ${newUser.userId}" }
        userRepository.save(newUser)
        return Response.Success(newUser)
    }
}
